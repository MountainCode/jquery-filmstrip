(function($) {
  $.fn.filmstrip = function(options) {
    var positions = [], cellIndex = 0;

    // Default settings.  Can be overridden by passing object to filmstrip function.
    var settings = $.extend({
      'begin-offset': '0',
      'cells-to-show': 3,
      'render-buttons': true,
      'filmstrip-selector': '.filmstrip',
      'controls-selector': '.filmstripControls',
      'css-move-axis': 'height',
      'cell-margin': 0,
      'transition-time': '1s',
      'move-by': 1,
      'child-selector': null,
      'back-button-selector': null,
      'ahead-button-selector': null,
      'move-event-name': 'filmstrip:move',
      'ahead-button-text': 'Next',
      'back-button-text': 'Previous',
      'ahead-end-of-list-text': 'End of list',
      'back-end-of-list-text': 'End of list',
      'side-by-side': 1
    }, options);

    var beginOffset = settings['begin-offset'], cellsToShow = settings['cells-to-show'], $filmstrip = $(settings['filmstrip-selector']);
    var cssMoveAxis = settings['css-move-axis'], transitionTime = settings['transition-time'], moveBy = settings['move-by'];
    var cssMoveProperty = 'height' === cssMoveAxis ? 'top' : 'left', childSelector = settings['child-selector'], moveEventName = settings['move-event-name'];

    var filmstrip = $filmstrip[0];
    if(0 === this.length || 0 === $filmstrip.length) return this;
    var $container = this;
    var cssTransitionsSupported = typeof filmstrip.style.transition !== 'undefined' || typeof filmstrip.style.webkitTransition !== 'undefined' || typeof filmstrip.style.MozTransition !== 'undefined';;

    var sum = 0;
    var $children = (childSelector ? $filmstrip.find(childSelector) : $filmstrip.children());
    $children.each(function(i, cell) {
      if(0 === i % settings['side-by-side']) {
        positions.push(sum);
        var offset = 'height' === cssMoveAxis ? $(cell).outerHeight() : $(cell).outerWidth();
        sum += offset + settings['cell-margin'];
      }
    });
    positions.push(sum);
    
    var resizeFilmstripContainerToCells = function() {
      var filmstripContainerDimension = positions[cellsToShow + cellIndex] - positions[cellIndex];
      // The container's height will be lost when cells are changed to position relative, so we set it explicitly here.
      if('width' === cssMoveAxis)
        $container.css('height', $container.innerHeight() + 'px');
      $container.css(cssMoveAxis, filmstripContainerDimension + 'px');
    };
    resizeFilmstripContainerToCells();

    // We don't want setting the initial dimensions to have a transition effect.
    setTimeout(function() {
      var containerTransitionValue = cssMoveAxis + ' ' + transitionTime;
      $container.css({
        'overflow': 'hidden',
        '-webkit-transition': containerTransitionValue,
        '-moz-transition': containerTransitionValue,
        'transition': containerTransitionValue
      });
    }, 100);

    var transitionValue = cssMoveProperty + ' ' + transitionTime;
    $filmstrip.css({
      'position': 'relative',
      '-webkit-transition': transitionValue,
      '-moz-transition': transitionValue,
      'transition': transitionValue
    });
    $filmstrip.css('margin-' + cssMoveProperty, beginOffset);
    $filmstrip.css(cssMoveProperty, 0); // Need this in Firefox as a value to transition from.

    $(document).on(moveEventName, function(e, opts) {
      var moveTo = cellIndex;
      if('ahead' === opts.direction && (cellIndex + cellsToShow) < positions.length)
        moveTo = (cellIndex + moveBy + cellsToShow < positions.length) ? cellIndex + moveBy : positions.length - (cellsToShow + 1);
      else if('back' === opts.direction && cellIndex > 0)
        moveTo = (cellIndex - moveBy >= 0) ? cellIndex - moveBy : 0;
      if(cssTransitionsSupported)
        $filmstrip.css(cssMoveProperty, -positions[moveTo]);
      else {
        if('top' === cssMoveProperty)
          $filmstrip.animate({'top': -positions[moveTo]}, 1000);
        else
          $filmstrip.animate({'left': -positions[moveTo]}, 1000);
      }
      cellIndex = moveTo;
      resizeFilmstripContainerToCells();
      setEnabledState();
    });

    var $backButton, $aheadButton, backButtonSelector = settings['back-button-selector'], aheadButtonSelector = settings['ahead-button-selector'];
    if(backButtonSelector && aheadButtonSelector) {
      $backButton = $(backButtonSelector);
      $aheadButton = $(aheadButtonSelector);
    }
    else {
      var $controls = $('<div class="controls"><button class="backButton">back</button><button class="aheadButton">ahead</button></div>');
      $backButton = $controls.find('.backButton');
      $aheadButton = $controls.find('.aheadButton');
      $(settings['controls-selector']).append($controls);
    }
    $backButton.on('click', function() {
      $(this).trigger(moveEventName, {'direction': 'back'});
      return false;
    });
    $aheadButton.on('click', function() {
      $(this).trigger(moveEventName, {'direction': 'ahead'});
      return false;
    });
    var setEnabledState = function() {
      if(0 === cellIndex) {
        $backButton.attr({'disabled': 'true', 'title': settings['back-end-of-list-text']});
        $backButton.addClass('disabled');
      }
      else {
        $backButton.removeAttr('disabled');
        $backButton.attr('title', settings['back-button-text']);
        $backButton.removeClass('disabled');
      }
      if(positions.length - cellsToShow <= cellIndex + 1) {
        $aheadButton.attr({'disabled': 'true', 'title': settings['ahead-end-of-list-text']});
        $aheadButton.addClass('disabled');
      }
      else {
        $aheadButton.removeAttr('disabled');
        $aheadButton.attr('title', settings['ahead-button-text']);
        $aheadButton.removeClass('disabled');
      }
    }
    setEnabledState();
    return this;
  };
})(jQuery);
