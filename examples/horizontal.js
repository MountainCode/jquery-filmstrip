jQuery(function($) {
  $('.filmstripContainer').filmstrip({
    'css-move-axis': 'width',
    'move-by': 3,
    'child-selector': '> li',
    'cell-margin': 4
  });
});
