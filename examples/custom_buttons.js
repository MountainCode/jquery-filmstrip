jQuery(function($) {
  $('.filmstripContainer').filmstrip({
    'move-by': 2,
    'child-selector': '> li',
    'cell-margin': 2,
    'ahead-button-selector': '.aheadButton',
    'back-button-selector': '.backButton'
  });
});
